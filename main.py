from tkinter import *

import gamestate
from gamestate import GameState


FIELD_SIZE = 50
CANVAS_WIDTH = gamestate.WIDTH * FIELD_SIZE
CANVAS_HEIGHT = gamestate.HEIGHT * FIELD_SIZE
GAME_OVER_TEMPLATE = "GAME OVER\n\nScore: {}"


class PythonSnake(Tk):
    def __init__(self):
        super().__init__()

        self.winfo_toplevel().title("Python Snake by StaNov")
        self.resizable(width=False, height=False)
        self.bind("<KeyPress>", lambda key: self.on_key_press(key.keysym))

        self.canvas = Canvas(self, width=CANVAS_WIDTH, height=CANVAS_HEIGHT, highlightthickness=0, bg="black")
        self.canvas.pack()

        self.game = GameState()
        self.game_update()

    def on_key_press(self, keysym):
        if keysym == "Escape":
            exit(0)

        self.game.set_current_direction(keysym)

    def game_update(self):
        self.game.tick()
        self.canvas.delete("all")

        self._draw_square_at(self.game.head_position, "green")
        self._draw_square_at(self.game.food_position, "red")
        self.canvas.create_text(10, 10, text="Score: " + str(self.game.score), fill="white", anchor="nw")

        if self.game.is_game_over:
            self._draw_game_over_text()

        self.after(500, self.game_update)

    def _draw_square_at(self, game_world_position, color):
        top_left_corner = tuple(map(lambda x: x * FIELD_SIZE, game_world_position))
        bottom_right_corner = tuple(map(lambda x: x + FIELD_SIZE, top_left_corner))
        self.canvas.create_rectangle(top_left_corner, bottom_right_corner, fill=color, outline="")

    def _draw_game_over_text(self):
        self.canvas.create_text(
            CANVAS_WIDTH / 2,
            CANVAS_HEIGHT / 2,
            text=GAME_OVER_TEMPLATE.format(self.game.score),
            fill="red",
            font=("", 50),
            justify="center"
        )


if __name__ == '__main__':
    PythonSnake().mainloop()
