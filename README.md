# Python Snake

A TDD project created while streaming live

![screenshot](images/screenshot_1.png)
![plan](images/python_snake_plan.png)

# Made with:
* tommy420iq
* OdpadKreativity
* RealTigerCZ
* DominikDerek
* lopata21
* HajdaM

# How to run

    sudo apt-get install -y python3-dev python3-pip python3-tk
    sudo pip3 install -r requirements.txt
    python3 main.py