_differences = {
    "Left": (-1, 0),
    "Right": (1, 0),
    "Up": (0, -1),
    "Down": (0, 1),
}

WIDTH = 15
HEIGHT = 10


class GameState:
    def __init__(self):
        self.is_game_over = False
        self.score = 0
        self.head_position = (0, 0)
        self.current_direction = "Right"
        self.food_position = (3, 3)

    def tick(self):
        if self.is_game_over:
            return

        position_difference = _differences[self.current_direction]
        self.head_position = tuple(map(sum, zip(self.head_position, position_difference)))

        if self._is_head_out_of_bounds():
            self.is_game_over = True

        if self.head_position == self.food_position:
            self.food_position = (4, 5)
            self.score += 1

    def set_current_direction(self, direction):
        if direction not in _differences.keys():
            return

        self.current_direction = direction

    def _is_head_out_of_bounds(self):
        return (
                self.head_position[0] < 0 or
                self.head_position[1] < 0 or
                self.head_position[0] >= WIDTH or
                self.head_position[1] >= HEIGHT
        )
