import pytest

from gamestate import GameState


def test_new_game():
    game = GameState()
    assert game.head_position == (0, 0)
    assert game.food_position == (3, 3)
    assert game.score == 0
    assert not game.is_game_over


def test_eaten_food_regenerates():
    game = GameState()
    game.head_position = (2, 2)
    game.set_current_direction("Right")
    game.food_position = (3, 2)

    game.tick()

    assert game.food_position != (3, 2)


def test_eaten_food_adds_point():
    game = GameState()
    game.head_position = (2, 2)
    game.set_current_direction("Right")
    game.food_position = (3, 2)
    game.score = 14

    game.tick()

    assert game.score == 15


def test_dont_move_when_game_over():
    game = GameState()
    game.head_position = (1, 1)
    game.is_game_over = True

    game.tick()

    assert game.head_position == (1, 1)


game_over_after_one_tick = [
    ((0, 2), "Left"),
    ((3, 0), "Up"),
    ((14, 2), "Right"),
    ((4, 9), "Down"),
]


@pytest.mark.parametrize("position, direction", game_over_after_one_tick)
def test_colliding_with_border_causes_game_over(position, direction):
    game = GameState()
    game.head_position = position
    game.set_current_direction(direction)

    game.tick()

    assert game.is_game_over
